// homework_16.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <ctime>

int getCurrentDay()
{
	time_t TSecNow = time(0);
	struct tm CurrentDateTime;
	localtime_s(&CurrentDateTime, &TSecNow);
	int Day = CurrentDateTime.tm_mday;
	return Day;
}

int main()
{
	const int N = 4;
	int array[N][N];
	int CurrentDay = getCurrentDay();
	int sum = 0;

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			array[i][j] = {i+j};
			std::cout << array[i][j] << " ";
		}
	std::cout << "\n";
	}
	std::cout << "\n";
	std::cout << "Current day is " << CurrentDay << "\n";

	for (int j = 0; j < N; j++)
	{
		sum += array[(CurrentDay % N)][j];
	}
	std::cout << "\n";
	std::cout << "Sum of elements in string " << (CurrentDay % N) << " is "<< sum << "\n";
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu tm_mday


// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
